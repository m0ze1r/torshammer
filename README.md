##Tors-Hammer

Welcome to the updated Tors-Hammer
Loose parts have been taken from "Dotfighter" and "Karl" 

##Usage
```
python
print "python2 torshammer.py -t <target> [-r <threads> -p <port> -T -h]"
-t|--target <Hostname|IP>
-r|--threads <Number of threads> Defaults to 256
-p|--port <Web Server Port> Defaults to 80
-T|--tor Enable anonymising through tor on 127.0.0.1:9050
-h|--help Shows this help
```

##Future Updates
I'm working on migrating the new Python Socks version v1.56 at some point but that's on a list of things to do
